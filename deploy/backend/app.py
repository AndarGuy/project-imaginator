from flask import Flask, request, session, send_file
import requests
import tempfile
import os

# Setting up backend server
app = Flask(__name__)

@app.route("/")
def hello_world():
    return "Hello World!"

@app.route("/imaginator")
def imaginator():
    data = request.json

    src, procedure = None, None

    if 'src' in data:
        src = data['src']
    else:
        return "You have to provide a link to a picture via SRC parameter."
    if 'procedure' in data:
        procedure = data['procedure']
    else:
        return "You have to provide a filter to be applied on a picture."

    with tempfile.NamedTemporaryFile(suffix='.bmp') as output:
        os.system(f'./imaginator --{procedure} ./examples/{src} {output.name}')
        return send_file(output.name, mimetype='image/bmp')
    

    return "Boops..."
